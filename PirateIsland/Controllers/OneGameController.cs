﻿using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using PirateIslandSrv;

namespace PirateIsland.Controllers
{
    public static class GameStorage
    {
        public static PirateIslandGame Game = new PirateIslandGame(5);
        public static readonly Dictionary<string, PirateIslandGame> Games = new Dictionary<string, PirateIslandGame>();

        public static PirateIslandGame Get(ClaimsPrincipal user)
        {
            if (!user.Identity.IsAuthenticated) return Game;

            var userId = user.FindFirst(ClaimTypes.NameIdentifier).Value;

            lock (Games)
            {
                if (Games.TryGetValue(userId, out var g))
                    return g;

                g = new PirateIslandGame(5);
                Games.Add(userId, g);
                return g;
            }
        }
    }


    public class OneGameController : Controller
    {
        public IActionResult Index()
        {
            return View(GameStorage.Get(User));
        }
    }
}