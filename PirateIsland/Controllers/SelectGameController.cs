﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using PirateIsland.Models;

namespace PirateIsland.Controllers
{
    public class SelectGameController : Controller
    {
        private readonly List<PirateIslandGame> _games = new List<PirateIslandGame>
        {
            new PirateIslandGame("01") { CurrentStage = PirateIslandGame.Stage.Starting }
        };


        public IActionResult Index()
        {
            return View(_games);
        }

        public IActionResult ShowGame(string guid)
        {
            ViewBag.GameId = guid;
            return View();
        }


        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
