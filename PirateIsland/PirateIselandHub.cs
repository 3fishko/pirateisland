﻿using System.Threading.Tasks;
using ca.axoninteractive.Geometry.Hex;
using PirateIsland.Controllers;

namespace PirateIsland
{
    public class PirateIselandHub : Microsoft.AspNetCore.SignalR.Hub
    {
        public Task OnClick(CubicHexCoord c)
        {
            return Clients.All.InvokeAsync("Show", new object[]
            {
                c,
                GameStorage.Get(Context.User).Tiles[c].Type.ToString()
            });
        }
    }
}
