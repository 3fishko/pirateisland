﻿using System;

namespace PirateIsland.Models
{
    public class PirateIslandGame
    {
        public enum Stage
        {
            Starting,
            InProgress,
            Finished
        }

        public string Id { get; } = Guid.NewGuid().ToString();

        public Stage CurrentStage { get; set; } = Stage.Starting;

        public DateTime CreateTime { get; } = DateTime.UtcNow;

        public string Author { get; }

        public PirateIslandGame(string author)
        {
            Author = author;
        }

        

    }
}
