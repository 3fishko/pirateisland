﻿using System.Collections.Generic;
using System.Linq;
using ca.axoninteractive.Geometry.Hex;
using IselandGenerator.HeightMap;
using Troschuetz.Random.Generators;

namespace IselandGenerator
{
    public class HexMap
    {
        public int IselandRadius { get; }
        public readonly Dictionary<CubicHexCoord, HexMapTile> Tiles;
        public readonly double[,] Source;


        public HexMap(int iselandRadius)
        {
            IselandRadius = iselandRadius;
            Tiles = CubicHexCoord.Area(new CubicHexCoord(), iselandRadius).ToDictionary(c=>c, c=>new HexMapTile(c));

            Scaler = new HexGrid(10);
            var minVec = MinVec;
            var maxVec = MaxVec;

            Source = new Radial(1.1, new StandardGenerator(300)).NewHeigthMap(
                (int) maxVec.x + 1, (int) maxVec.y + 1);

            foreach (var t in Tiles.Values)
            {
                var pt = Scaler.OffsetToPoint(t.Coord.ToOffset()) - minVec;
                t.IsGround = Source[(int) pt.x, (int) pt.y] > 0;
            }

            var newtiles = CubicHexCoord.Ring(new CubicHexCoord(), iselandRadius + 1).Select(c => new HexMapTile(c)).ToList();
            newtiles.ForEach(t=>Tiles.Add(t.Coord, t));

            SetBorderHeight();
            for (int h = 2; SetHeight(h); ++h) { }
            for (int h = -2; SetDeep(h); --h) { }
        }

        public static HexGrid Scaler = new HexGrid(10);

        public Vec2D MinVec
        {
            get
            {
                var minx = Tiles.Values.Min(t => Scaler.OffsetToPoint(t.Coord.ToOffset()).x);
                var miny = Tiles.Values.Min(t => Scaler.OffsetToPoint(t.Coord.ToOffset()).y);
                return new Vec2D(minx,miny);
            }
        }

        public Vec2D MaxVec
        {
            get
            {
                var minx = Tiles.Values.Max(t => Scaler.OffsetToPoint(t.Coord.ToOffset()).x);
                var miny = Tiles.Values.Max(t => Scaler.OffsetToPoint(t.Coord.ToOffset()).y);
                return new Vec2D(minx,miny) - MinVec;
            }
        }

        public int MinDeep => Tiles.Values.Min(t => t.Height);
        public int MaxHeight => Tiles.Values.Max(t => t.Height);
        
        private void SetBorderHeight()
        {
            foreach (var t in Tiles.Values)
            {
                if (t.Height != 0) continue;
                var nbs = CubicHexCoord.Ring(t.Coord, 1);
                foreach (var nbcoord in nbs)
                {
                    if (!Tiles.TryGetValue(nbcoord, out var nbtile)) continue;
                    if (nbtile.IsGround != !t.IsGround) continue;

                    t.Height = t.IsGround ? 1 : -1;
                    nbtile.Height = nbtile.IsGround ? 1 : -1;
                    break;
                }
            }
        }

        private bool SetHeight(int h)
        {
            bool res = false;
            foreach (var t in Tiles.Values)
            {
                if (t.Height != 0) continue;
                if (!t.IsGround) continue;
                var nbs = CubicHexCoord.Ring(t.Coord, 1);
                foreach (var nbcoord in nbs)
                {
                    if (!Tiles.TryGetValue(nbcoord, out var nbtile)) continue;
                    if (nbtile.IsGround != t.IsGround) continue;
                    if (nbtile.Height != h - 1) continue;

                    t.Height = h;
                    res = true;
                    break;
                }
            }

            return res;
        }

        private bool SetDeep(int h)
        {
            bool res = false;
            foreach (var t in Tiles.Values)
            {
                if (t.Height != 0) continue;
                if (t.IsGround) continue;
                var nbs = CubicHexCoord.Ring(t.Coord, 1);
                foreach (var nbcoord in nbs)
                {
                    if (!Tiles.TryGetValue(nbcoord, out var nbtile)) continue;
                    if (nbtile.IsGround != t.IsGround) continue;
                    if (nbtile.Height != h + 1) continue;

                    t.Height = h;
                    res = true;
                    break;
                }
            }

            return res;
        }
    }

    public class HexMapTile
    {
        public CubicHexCoord Coord { get; }
        //public Vec2D Coord2 { get; set; }

        public bool IsGround;
        public int Height = 0;

        public HexMapTile(CubicHexCoord coord)
        {
            Coord = coord;
        }

        public override string ToString()
        {
            return $"[{Coord.x}, {Coord.y}, {Coord.z}]";
        }
    }
}
