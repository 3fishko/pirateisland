﻿using System.Drawing;
using System.IO;

namespace IselandGenerator
{
    class Class1
    {
        public static Brush[] GrayColors = new Brush[256];
        public static Brush[] WaterColors = new Brush[256];
        public static Brush[] GroundColors = new Brush[256];

        public static void Main(string[] args)
        {
            for (int g = 0; g < 256; ++g)
            {
                GrayColors[g] = new SolidBrush(Color.FromArgb(g, g, g));
                WaterColors[g] = new SolidBrush(Color.FromArgb(0, 0, g));
                GroundColors[g] = new SolidBrush(Color.FromArgb(0, g, 0));
            }

            try { Directory.Delete("output", true); } catch {  /* skip it */ }

            var hm = new HexMap(10);
            WriteMap(hm.Source, "source");
            WriteMap(hm, "hexmap");

        }

        private static void WriteMap(HexMap map, string file)
        {
            PointF[] ptsHex = {
                new PointF(2*8.66f, 15), 
                new PointF(8.66f,   20), 
                new PointF(0,       15), 
                new PointF(0,       5), 
                new PointF(8.66f,   0), 
                new PointF(2*8.66f, 5), 
            };
            var maxVec = map.MaxVec;
            var minVec = map.MinVec;
            using (var bmp = new Bitmap((int) maxVec.x + 20, (int) maxVec.y + 20))
            {
                using (Graphics gfx = Graphics.FromImage(bmp))
                    foreach (var t in map.Tiles.Values)
                    {
                        // int baseColor = t.IsGround ? 0 : 100;
                        var mapMinDeep = map.MinDeep;
                        var mapDifferential = map.MaxHeight-mapMinDeep;
                        int baseColor = (t.Height - mapMinDeep)*255/mapDifferential;

                        var ptsNow = (PointF[])ptsHex.Clone();
                        for (int i = 0; i < ptsNow.Length; i++)
                        {
                            ptsNow[i].X += HexMap.Scaler.OffsetToPoint(t.Coord.ToOffset()).x - minVec.x;
                            ptsNow[i].Y += HexMap.Scaler.OffsetToPoint(t.Coord.ToOffset()).y - minVec.y;
                        }
                        var brush = (t.IsGround ? GroundColors : WaterColors)[baseColor];
                        gfx.FillPolygon(brush, ptsNow);
                    }

                Directory.CreateDirectory("output");
                bmp.Save($"output/{file}.png");
            }
        }

        private static void WriteMap(double[,] map, string file)
        {
            using (var bmp = new Bitmap(map.GetLength(0), map.GetLength(1)))
            {
                using (Graphics gfx = Graphics.FromImage(bmp))
                    for (int x = 0; x < map.GetLength(0); x++)
                    for (int y = 0; y < map.GetLength(1); y++)
                    {
                        int baseColor = 255 - (int) (map[x, y] * 255);
                        var brush = GrayColors[baseColor];
                        gfx.FillRectangle(brush, x, y, 1, 1);
                    }

                Directory.CreateDirectory("output");
                bmp.Save($"output/{file}.png");
            }
        }
    }
}
