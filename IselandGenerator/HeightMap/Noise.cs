﻿using System;

namespace IselandGenerator.HeightMap
{
    public class Noise : IHeigthMapGenerator
    {
        public Noise(double persistence, int octaves)
        {
            Persistence = persistence;
            Octaves = octaves;
        }

        public double[,] Terra { get; set; }

        public double[,] Generate()
        {
            var octaves = Octaves;
            for (int k = 0; k < octaves; k++)
                octave(k);

            return Terra;
        }

        public int Octaves { get; }

        private void octave(int octave)
        {
            var freq = (int)Math.Pow(2, octave);
            var amp = Math.Pow(Persistence, octave);

            double[,] arr = new double[freq + 1, freq + 1];
            for (var j = 0; j < freq + 1; j++)
            {
                for (var i = 0; i < freq + 1; i++)
                {
                    arr[j, i] = R.NextDouble() * amp;
                }
            }

            var nx = Terra.GetLength(0) / freq + 1;
            var ny = Terra.GetLength(1) / freq + 1;

            for (var ky = 0; ky < Terra.GetLength(1); ky++)
            {
                for (var kx = 0; kx < Terra.GetLength(0); kx++)
                {
                    var i = kx / nx;
                    var j = ky / ny;
                    var i1 = i + 1;
                    var j1 = j + 1;

                    var dx0 = kx - i * nx;
                    var dx1 = nx - dx0;
                    var dy0 = ky - j * ny;
                    var dy1 = ny - dy0;
                    var z = (arr[j, i] * dx1 * dy1
                             + arr[j, i1] * dx0 * dy1
                             + arr[j1, i] * dx1 * dy0
                             + arr[j1, i1] * dx0 * dy0)
                            / (nx * ny);

                    Terra[kx, ky] += z;
                }
            }
        }

        public Random R { get; } = new Random();

        public double Persistence { get; }

        public double[,] NewHeigthMap(int nHeigth, int nWidth)
        {
            Terra = new double[nWidth,nHeigth];
            Generate();
            return Terra;
        }
    }
}