﻿using System;
using Troschuetz.Random;
using Troschuetz.Random.Generators;

namespace IselandGenerator.HeightMap
{
    public class DiamondSquare : IHeigthMapGenerator
    {

        #region Public

        /// <summary>
        /// Async implementation of Generate
        /// </summary>
        /// <returns>Task will returns double array of double</returns>
        public double[,] Generate()
        {
            Terra = new double[Size, Size];

            var last = Size - 1;
            Terra[0, 0] = _getOffset(Size);
            Terra[0, last] = _getOffset(Size);
            Terra[last, 0] = _getOffset(Size);
            Terra[last, last] = _getOffset(Size);

            _divide(Size);

            return Terra;
        }

        #endregion

        #region Main algorithm methods

        private void _divide(int stepSize)
        {
            double a;
            double b;
            double c;
            double d;

            int half;
            while ((half = stepSize / 2) >= 1)
            {
                for (var x = half; x < Size; x += stepSize)
                {
                    for (var y = half; y < Size; y += stepSize)
                    {
                        // square
                        a = _getCellHeight(x - half, y - half, half);
                        b = _getCellHeight(x + half, y + half, half);
                        c = _getCellHeight(x - half, y + half, half);
                        d = _getCellHeight(x + half, y - half, half);
                        var average = (a + b + c + d) / 4;
                        Terra[x, y] = average + _getOffset(stepSize);

                        _diamond(x, y - half, half);
                        _diamond(x - half, y, half);
                        _diamond(x, y + half, half);
                        _diamond(x + half, y, half);
                        // _square(x, y, half);
                    }
                }

                stepSize = half;
            }
        }
        
        private void _diamond(int x, int y, int size)
        {
            var offset = _getOffset(size);
            var a = _getCellHeight(x, y - size, size);
            var b = _getCellHeight(x, y + size, size);
            var c = _getCellHeight(x - size, y, size);
            var d = _getCellHeight(x + size, y, size);
            var average = (a + b + c + d) / 4;
            Terra[x, y] = average + offset;
        }

        #endregion

        #region Additional methods

        /// <summary>
        /// Get random offset. Value depends on current step of divide.
        /// </summary>
        /// <param name="stepSize"></param>
        /// <returns></returns>
        private double _getOffset(int stepSize)
        {
            var offset = (double)stepSize / Size * R.Next(-Size, Size);
            var sign = offset < 0 ? -1 : 1;
            return sign * Math.Pow(Math.Abs(offset), 1 / Math.Sqrt(Persistence));
        }

        /// <summary>
        /// Getting height of cell by indexes. Random if cell out of map.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="stepSize"></param>
        /// <returns></returns>
        private double _getCellHeight(int x, int y, int stepSize = 0)
        {
            if (x < 0 || y < 0 || x >= Size || y >= Size)
                return _getOffset(stepSize);
            return Terra[x, y];
        }

        #endregion

        private int Size;
        private double[,] Terra;
        public double Persistence { get; }
        public IGenerator R { get; }

        public DiamondSquare(double persistence, IGenerator random = null)
        {
            Persistence = persistence;
            R = random ?? new StandardGenerator();
        }

        public double[,] NewHeigthMap(int nHeigth, int nWidth)
        {
            Size = nHeigth;
            CheckSize();

            return Generate();
        }

        private void CheckSize()
        {
            if (Size % 2 != 1)
                throw new ArgumentException("nHeigth must be 2^n + 1");

            int s = Size >> 1;
            while (s != 0)
            {
                if (s == 1) return;
                if (s % 2 == 1)
                    throw new ArgumentException("nHeigth must be 2^n + 1");
                s = s >> 1;
            }
        }
    }
}