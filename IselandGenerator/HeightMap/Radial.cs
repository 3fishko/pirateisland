﻿using System;
using System.Numerics;
using Troschuetz.Random;
using Troschuetz.Random.Generators;

namespace IselandGenerator.HeightMap
{
    public class Radial : IHeigthMapGenerator
    {
        public Radial(double islandFactor = 1.07, IGenerator r = null)
        {
            _islandFactor = islandFactor;
            _r = r ??  new StandardGenerator();
        }

        // 1.0 means no small islands; 2.0 leads to a lot
        private readonly double _islandFactor;
        private readonly IGenerator _r;

        public double[,] NewHeigthMap(int nHeigth, int nWidth)
        {
            var bumps = _r.Next(1, 6);
            var startAngle = _r.NextDouble(0, 2 * Math.PI);
            var dipAngle = _r.NextDouble(0, 2 * Math.PI);
            var dipWidth = _r.NextDouble(0.2, 0.7);

            double vlen(Vector2 v)
            {
                return 0.5 * (Math.Max(Math.Abs(v.X), Math.Abs(v.Y)) + v.Length());
            }

            var islandFactor = vlen(new Vector2(nHeigth, nWidth))/2.5;

            bool Inside(Vector2 q)
            {
                var angle = Math.Atan2(q.Y, q.X);
                var length = vlen(q) / islandFactor;

                double r1;
                double r2;
                // if (Math.Abs(angle - dipAngle) < dipWidth ||
                //     Math.Abs(angle - dipAngle + 2 * Math.PI) < dipWidth ||
                //     Math.Abs(angle - dipAngle - 2 * Math.PI) < dipWidth)
                //     r1 = r2 = 0.2;
                // else
                {
                    r1 = 0.5 + 0.40 * Math.Sin(startAngle + bumps * angle + Math.Cos((bumps + 3) * angle));
                    r2 = 0.7 - 0.20 * Math.Sin(startAngle + bumps * angle - Math.Sin((bumps + 2) * angle));
                   //  if (r2 > 0.9) r2 = 0.9;
                   //  if (r1 > r2) r1 = r2;
                }

                if (length < r1) return true;
                return r1 * _islandFactor < length && length < r2;
            }

            var map = new double[nHeigth, nWidth];

            for (int x = 0; x < nHeigth; x++)
            for (int y = 0; y < nWidth; y++)
            {
                map[x, y] = Inside(new Vector2(x - nHeigth / 2, y - nWidth / 2)) ? 1 : 0;
            }

            return map;
        }
    }
}