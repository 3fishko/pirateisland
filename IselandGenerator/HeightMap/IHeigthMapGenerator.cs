﻿using System;

namespace IselandGenerator.HeightMap
{
    /// <summary>
    /// Генератор карты высот
    /// </summary>
    public interface IHeigthMapGenerator
    {
        /// <summary>
        /// Новая карта высот
        /// </summary>
        double[,] NewHeigthMap(int nHeigth, int nWidth);
    }

    public static class HeigthMapGeneratorExtentions
    {
        public static double[,] Normalize(this double[,] map)
        {
            const double minValue = 0.0;
            const double maxValue = 1.0;

            var newmap = new double[map.GetLength(0), map.GetLength(1)];

            double minGen = Double.MaxValue;
            double maxGen = Double.MinValue;

            for (int x = 0; x < map.GetLength(0); x++)
            for (int y = 0; y < map.GetLength(1); y++)
            {
                var v = map[x, y];
                if (v < minGen) minGen = v;
                if (v > maxGen) maxGen = v;
            }

            double k = (maxValue - minValue) / (maxGen - minGen);
            for (int x = 0; x < map.GetLength(0); x++)
            for (int y = 0; y < map.GetLength(1); y++)
            {
                var v = minValue + k * (map[x, y] - minGen);
                // против ошибок округления:
                if (v < minValue) v = minValue;
                if (v > maxValue) v = maxValue;
                newmap[x, y] = v;
            }

            return newmap;
        }
    }
}
