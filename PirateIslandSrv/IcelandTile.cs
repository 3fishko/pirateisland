﻿using System.Collections.Generic;
using ca.axoninteractive.Geometry.Hex;

namespace PirateIslandSrv
{
    public class IcelandTile
    {
        public enum TileType
        {
            Grass, // nothing
            Cactus, // stop turn
            Cannon, // kill enemy using cannonball
            DirectionIndicator, // roll for direction
            Crocodile, // lose pirate
            Savages, // lock pirate
            Volcano, // move pirate to sea
            Treasure,
            Bog
        }

        public readonly CubicHexCoord Coord;
        public readonly TileType Type;

        public List<Item> Items { get; } = new List<Item>();
        public List<Character> Characters { get; } = new List<Character>();
        public CubicHexCoord Direction { get; set; }
        public int BogSize { get; set; }

        public IcelandTile(CubicHexCoord coord, TileType type)
        {
            Coord = coord;
            coord.ToOffset();
            Type = type;

            var map = new HexGrid(53);
            VC = map.AxialToPoint(coord.ToAxial());
        }

        public Vec2D VC { get; }
    }
}