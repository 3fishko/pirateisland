﻿using System;
using System.Collections.Generic;
using System.Linq;
using ca.axoninteractive.Geometry.Hex;

namespace PirateIslandSrv
{
    internal class ShipCrew
    {
        
    }

    public class PirateIslandGame
    {
        public const float HexRadius = 10f;
        private readonly HexGrid _iselandMap = new HexGrid(HexRadius);
        public readonly int IselandRadius;
        public readonly Dictionary<CubicHexCoord, IcelandTile> Tiles = new Dictionary<CubicHexCoord, IcelandTile>();

        private readonly Dictionary<IcelandTile.TileType, double> _typesFreqencies = new Dictionary<IcelandTile.TileType, double>
        {
            { IcelandTile.TileType.Cactus,             18/169d},
            { IcelandTile.TileType.Bog,                14/169d},
            { IcelandTile.TileType.Cannon,              1/169d},
            { IcelandTile.TileType.Crocodile,           4/169d},
            { IcelandTile.TileType.DirectionIndicator, 12/169d},
            { IcelandTile.TileType.Savages,             6/169d},
            { IcelandTile.TileType.Volcano,            14/169d},
            { IcelandTile.TileType.Treasure,           38/169d}
        };

        private readonly Dictionary<int, double> _bogFreqencies = new Dictionary<int, double>
        {
            { 2, 7/14d},
            { 3, 4/14d},
            { 4, 3/14d}
        };

        private readonly Dictionary<int, double> _treasureFreqencies = new Dictionary<int, double>
        {
            { 1, 8/38d},
            { 2, 10/38d},
            { 3, 8/38d},
            { 4, 6/38d},
            { 5, 4/38d},
            { 6, 2/38d}
        };

        public PirateIslandGame(int iselandRadius)
        {
            IselandRadius = iselandRadius;
            var area = CubicHexCoord.Area(new CubicHexCoord(), iselandRadius);
            var tileTypes = RandomUtils.GetRandomValuesList(area.Length, _typesFreqencies, IcelandTile.TileType.Grass);
            int itt = 0;
            Tiles = area.ToDictionary(c=>c, c=>new IcelandTile(c, tileTypes[itt++]));

            var directionRing = CubicHexCoord.Ring(new CubicHexCoord(), 1);

            var typedTiles = Enum.GetValues(typeof(IcelandTile.TileType)).OfType<IcelandTile.TileType>()
                    .ToDictionary(t => t, t => new List<IcelandTile>());
            foreach (var tile in Tiles.Values)
            {
                typedTiles[tile.Type].Add(tile);
                if (tile.Type == IcelandTile.TileType.DirectionIndicator || tile.Type == IcelandTile.TileType.Volcano)
                    tile.Direction = directionRing.Random();
            }

            var treasures = RandomUtils.GetRandomValuesList(typedTiles[IcelandTile.TileType.Treasure].Count, _treasureFreqencies, 1);
            int i = 0;
            foreach (var tile in typedTiles[IcelandTile.TileType.Treasure])
                tile.Items.AddRange(Enumerable.Repeat(new Item { Type = Item.ItemType.Gold }, treasures[i++]));
            var bogs = RandomUtils.GetRandomValuesList(typedTiles[IcelandTile.TileType.Bog].Count, _bogFreqencies, 2);
            i = 0;
            foreach (var tile in typedTiles[IcelandTile.TileType.Bog])
                tile.BogSize = bogs[i++];

            List<object> bonuses = new List<object>();
            bonuses.AddRange(Enumerable.Repeat(new Item {Type = Item.ItemType.CannonBall}, 2));
            bonuses.AddRange(Enumerable.Repeat(new Item {Type = Item.ItemType.Rum }, 9));
            bonuses.Add(new Item {Type = Item.ItemType.Wheelbarrow });
            bonuses.Add(new Item {Type = Item.ItemType.Spyglass });
            bonuses.Add(new Character(Character.CharacterType.BenGun));
            bonuses.Add(new Character(Character.CharacterType.Missionary));
            if (typedTiles[IcelandTile.TileType.Grass].Count > bonuses.Count)
                bonuses.AddRange(Enumerable.Repeat<object>(null, typedTiles[IcelandTile.TileType.Grass].Count - bonuses.Count));
            bonuses.Shuffle();
            i = 0;
            foreach (var tile in typedTiles[IcelandTile.TileType.Grass])
            {
                switch (bonuses[i++])
                {
                    case Character ch: tile.Characters.Add(ch); break;
                    case Item it: tile.Items.Add(it); break;
                }
            }
        }
    }
}