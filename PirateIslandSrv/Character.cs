﻿namespace PirateIslandSrv
{
    public class Character
    {
        public enum CharacterType
        {
            Pirate1,
            Pirate2,
            Pirate3,
            BenGun,
            Missionary
        }

        public Item Item { get; set; }

        public CharacterType Type { get; }

        public bool InCaptivity; // locked by savages
        public bool Buxi; // Invulnerable from rum

        public Character(CharacterType characterType)
        {
            Type = characterType;
        }
    }
}