﻿using System.Collections.Generic;

namespace PirateIslandSrv
{
    internal class Ship
    {
        public readonly List<Item> Items = new List<Item>();
        public readonly List<Character> Characters = new List<Character>();
    }
}