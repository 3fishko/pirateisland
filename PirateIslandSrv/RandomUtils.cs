﻿using System;
using System.Collections.Generic;
using System.Linq;
using Troschuetz.Random;
using Troschuetz.Random.Generators;

namespace PirateIslandSrv
{
    public static class RandomUtils
    {
        static readonly IGenerator Rand = new ALFGenerator();

        public static double Next()
        {
            return Rand.NextDouble();
        }

        public static double NextAngle()
        {
            return 2 * Next() * Math.PI;
        }

        public static T Random<T>(this IList<T> list)
        {
            int i = Rand.Next(list.Count);
            return list[i];
        }

        public static void Shuffle<T>(this IList<T> characters)
        {
            for (int i = characters.Count - 1; i > 1; --i)
            {
                int j = Next(i);
                var tmp = characters[j];
                characters[j] = characters[i];
                characters[i] = tmp;
            }
        }

        public static int Next(int maxValue)
        {
            return Rand.Next(maxValue);
        }

        public static bool Roll(double probablility)
        {
            return Rand.NextDouble() < probablility;
        }

        public static int SelectDiscreteIndex(double[] frqs)
        {
            var p = Rand.NextDouble();
            for (int i = 0; i < frqs.Length; i++)
            {
                p -= frqs[i];
                if (p < 0) return i;
            }
            return -1;
        }

        public static List<T> GetRandomValuesList<T>(int size, Dictionary<T, double> frequencies, T other = default(T))
        {
            var list = new List<T>(size);
            foreach (var type in frequencies)
                list.AddRange(Enumerable.Repeat(type.Key, (int)Math.Ceiling(type.Value * size)));
            if (size > list.Count)
                list.AddRange(Enumerable.Repeat(other, size - list.Count));
            list.Shuffle();
            return list;
        }
    }
}
