﻿namespace PirateIslandSrv
{
    public class Item
    {
        public enum ItemType
        {
            Gold,
            CannonBall,
            Rum,
            Wheelbarrow,
            Spyglass
        }

        public ItemType Type;
    }
}